from django.db import models
# from gettext import gettext _

# Create your models here.

CATEGORIES = (
    (0, 'auto'),
    (1, 'bikes'),
    (2, 'chips')
)



class Product(models.Model):
    name = models.CharField('gggg', max_length=100)
    price = models.FloatField()
    category = models.PositiveIntegerField(choises=CATEGORIES)

    def __repr__(self):
        return "Product({})".format(self. name)

    def __str__(self):
        return self.name
