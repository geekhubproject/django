from django.http.response import HttpResponse
from django.shortcuts import render
from random import randint

# Create your views here.
def index(request):
    return render(request, 'index.html', {'rand': randint(1,10)})
