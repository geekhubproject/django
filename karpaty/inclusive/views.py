from django.shortcuts import render, render_to_response
from inclusive.models import FreeHotelServicesHeader, FreeHotelServices, imgi


# Create your views here.
def freeservices(request):
    # return render(request, 'rooms.html')
    return render_to_response('inclusive.html', {
        'FreeHotelServices': FreeHotelServices.objects.all(),
        'FreeHotelServicesHeader': FreeHotelServicesHeader.objects.all(),
        'imgi': imgi.objects.all()
    })