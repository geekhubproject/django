from django.apps import AppConfig


class InclusiveConfig(AppConfig):
    name = 'inclusive'
