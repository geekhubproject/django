from django.db import models
from sorl.thumbnail import ImageField

# Create your models here.

class FreeHotelServicesHeader(models.Model):
    header = models.CharField(max_length=100, null=True, verbose_name='Заголовок')
    class Meta:
        verbose_name_plural = 'FreeServices Заголовок'
        verbose_name = 'FreeServices Заголовок'


class FreeHotelServices(models.Model):
    img = models.ImageField(upload_to='inclusive', verbose_name='free service image')
    teaser = models.CharField(max_length=100, null=True, verbose_name='Короткий текст')
    full_text = models.TextField(max_length=1000, null=True, verbose_name='Полный текст')

    class Meta:
        verbose_name_plural = 'FreeServices'
        verbose_name = 'FreeServices'

    def __str__(self):
        return self.teaser


class imgi(models.Model):
    img = models.ImageField(upload_to='inclusive1', verbose_name='free service image')
    class Meta:
        verbose_name_plural = 'tstimg'
        verbose_name = 'tstimg'