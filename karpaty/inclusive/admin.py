from django.contrib import admin

# Register your models here.
from inclusive.models import FreeHotelServicesHeader, FreeHotelServices, imgi
from sorl.thumbnail.admin import AdminImageMixin


# Register your models here.

class FreeHotelServicesAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ['teaser']

admin.site.register(FreeHotelServicesHeader)
admin.site.register(FreeHotelServices)
admin.site.register(imgi)