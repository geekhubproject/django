from django.conf.urls import url
from django.contrib import admin
from . import views


admin.autodiscover()
app_name = 'services'
urlpatterns = [
    url(r'^', views.freeservices, name='freeservices'),
]