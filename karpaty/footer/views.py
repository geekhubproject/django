from django.shortcuts import render, render_to_response
from footer.models import Contacts
# Create your views here.

def footer(request):
    return render_to_response('footer.html', {
        'Contacts': Contacts.objects.all()
    })