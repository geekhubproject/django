
from django.conf.urls import url
from django.contrib import admin
from . import views


from django.conf import settings

admin.autodiscover()
app_name = 'footer'
urlpatterns = [
    url(r'^', views.footer, name='footer'),
]
