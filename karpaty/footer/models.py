# -*- coding: utf-8 -*-
#from __future__ import unicode_literals

from django.db import models
# from django.conf import settings

# Create your models here.

class Contacts(models.Model):
    head = models.CharField(max_length=100, null=True, verbose_name='contact header')
    adress = models.TextField(max_length=1000, null=True, verbose_name='contact adress')
    phone = models.CharField(max_length=100, null=True, verbose_name='contact phone')
    gps_coordinates = models.CharField(max_length=100, null=True, verbose_name='contact gps coordinates')

    class Meta:
        verbose_name_plural = 'Контакты'
        verbose_name = 'Контакты'

    def __str__(self):
        return self.head