from django.db import models

# Create your models here.


class BannerTextRooms(models.Model):
    banner_text = models.TextField(blank=True, null=True, max_length=1000, verbose_name='Текст баннера отелей')

    class Meta:
        verbose_name_plural = 'Текст баннера отелей'
        verbose_name = 'Текст баннера отелей'

    def __str__(self):
        return self.banner_text