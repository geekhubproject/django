# from django.shortcuts import render, HttpResponse, loader, render_to_response
from rooms.models import BannerTextRooms

from django.shortcuts import render, redirect, render_to_response
from django.http.response import HttpResponse
from django.contrib import auth



# Create your views here.

def viewroom(request):
    # return render(request, 'rooms.html')
    return render_to_response('rooms.html', {
        'BannerTextRooms': BannerTextRooms.objects.all()
    })
