
from django.conf.urls import url
from django.contrib import admin
from . import views


from django.conf import settings

admin.autodiscover()
app_name = 'rooms'
urlpatterns = [
    url(r'^', views.viewroom, name='viewroom'),
]

#
# urlpatterns = [
#     url(r'^', 'blocks.views.show_blocks'),
# ]