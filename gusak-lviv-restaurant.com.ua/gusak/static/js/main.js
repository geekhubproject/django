/**
 * Created by Developer on 27.04.2017.
 */
$(document).ready(function(){

      $('.photogallery').slick({
          infinite: true,
          slidesToShow: 5,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
                settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
      $('.fancybox').fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });

      $('.menu-slider').slick();

      function scrollNav() {
          $('.nav a').click(function(){
            //Toggle Class
            $(".active").removeClass("active");
            $(this).closest('li').addClass("active");
            var theClass = $(this).attr("class");
            $('.'+theClass).parent('li').addClass('active');
            //Animate
            $('html, body').stop().animate({
                scrollTop: $( $(this).attr('href') ).offset().top - 160
            }, 400);
            return false;
          });
          $('.scrollTop a').scrollTop();
        }
        scrollNav();

    });