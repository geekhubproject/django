from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin

from landing.models import Header, AboutUs, Photogallery, Menu
# Register your models here.

@admin.register(Photogallery)
class PhotoGalleryAdmin(AdminImageMixin, admin.ModelAdmin):
    pass

@admin.register(Menu)
class MenuAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ['id', 'name', 'description']
    list_editable = ['name', 'description']

admin.site.register(Header)
admin.site.register(AboutUs)
# admin.site.register(AdminImageMixin, Photogallery, )