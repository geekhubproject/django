from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail import ImageField
# Create your models here.

class Header(models.Model):
    header = models.CharField('Заголовок', max_length=100)
    slug = models.CharField('Краткое описание', max_length=500)

    class Meta:
        verbose_name_plural = 'Заголовок'
        verbose_name = 'Заголовок'

    def __str__(self):
        return self.header



class AboutUs(models.Model):
    header = models.CharField('Про нас', max_length=100)
    slug = RichTextField()

    class Meta:
        verbose_name_plural = 'Про нас'
        verbose_name = 'Про нас'

    def __str__(self):
        return self.header

class Photogallery(models.Model):
    photo = ImageField(upload_to='photogallery', verbose_name='Галерея')

    class Meta:
        verbose_name_plural = 'Галерея'
        verbose_name = 'Галерея'


class Menu(models.Model):
    name = models.CharField('Название', max_length=100)
    description = RichTextField()
    img = ImageField(upload_to='menu_photo', verbose_name='Фото меню')

    class Meta:
        verbose_name_plural = 'Меню'
        verbose_name = 'Меню'
    def __str__(self):
        return self.name