from django.shortcuts import render, HttpResponseRedirect
from django.contrib import messages
from gusak import settings

from .models import Header, AboutUs, Photogallery, Menu
# Create your views here.

def temp(request):
    context = {
        'header': Header.objects.all(),
        'aboutus': AboutUs.objects.all(),
        'gallery': Photogallery.objects.all(),
        'menu': Menu.objects.all(),

    }
    return render(request, 'index.html', context=context)

def email_table(request):
    name = request.POST.get('name', None)
    amount = request.POST.get('amount', None)
    date = request.POST.get('date', None)
    time = request.POST.get('time', None)
    phone = request.POST.get('phone', None)
    comment = request.POST.get('comment', None)

    if sendMail(name, amount, date, time, phone, comment):
        messages.success(request, 'Мы скоро перезвоним Вам')
        return HttpResponseRedirect('/')
    else:
        messages.error(request, 'Ошибка в отправке запроса. Попробуйте позже.')
        return HttpResponseRedirect('/')

def sendMail(name, amount, date, time, phone, comment):
    # -*- coding: utf-8 -*-
    from django.core.mail import send_mail

    text = u'Заказ столика Тлустый гусак: \n' + 'Имя: ' + name + '\nКоличество: ' + amount + '\nДата: ' + date + '\nВремя: ' + time + '\nТелефон: ' + phone + '\nКомментарий: ' + comment
    return send_mail('Zakaz stolika Tlustiy gusak', text, settings.EMAIL_HOST_USER, ['andrey.magaleckiy@hg.reikartz.com','tlustiy.gusak@reikartz.com','diana.bessarab@reikartz.com','anna.tarasova@reikartz.com'], fail_silently=False)