# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-27 12:24
from __future__ import unicode_literals

from django.db import migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0002_auto_20170427_1451'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photogallery',
            name='photo',
            field=sorl.thumbnail.fields.ImageField(upload_to='photogallery', verbose_name='Галерея'),
        ),
    ]
